$(document).ready(function(){

  // тултип
  function simple_tooltip(target_items, name){
    $(target_items).each(function(i){
      $("body").append("<div class='"+name+"' id='"+name+i+"'><p>"+$(this).attr('title')+"</p></div>");
      var my_tooltip = $("#"+name+i);

      $(this).removeAttr("title").mouseover(function(){
        my_tooltip.css({display:"none"}).fadeIn(400);
      }).mousemove(function(kmouse){
        my_tooltip.css({left:kmouse.pageX, top:kmouse.pageY-30});
      }).mouseout(function(){
        my_tooltip.fadeOut(400);
      });
    });
  }

  $.getJSON("../js/districts.json", function(data) {

    // добавляю новые элементы в DOM
    var root = $('.districts'),
    box;
    for (var k in data) {
        if (data.hasOwnProperty(k)) {
            box = $('<div class="' + k + '"></div>');
            for (var i in data[k].img) {
                if (data[k].img.hasOwnProperty(i)) {
                    $('<div class="popup"></div>')
                        .html('<img src="/img/' + k + '/' + data[k].img[i].name + '.png" />')
                        .appendTo(box);
                }
            }
            box.appendTo(root);
        }
    }

    // задаю общие координаты для каждой зоны
    function popup_xy(district, x1, y1) {
      var name = "." + district + " .popup"
      $(name).each(function(i, el) {

          $(el).css({left : x1 + "%", top : y1 + "%"});

      })
    }
    popup_xy("armalit", 23, 34);
    popup_xy("burevestnik", 20, 57 );
    popup_xy("skbk", 8, 65 );
    popup_xy("askold", 80, 78 );
  });

  // получаю координаты городов
  $.getJSON("../js/cities.json", function(data) {
    for (var i in data) {
      var row = data[i];
      var city = i;
      $('.points').append("<div class='points__item' id='" + city + "'></div>");
      $("#" + city).attr("title", row.rus);
      $("#" + city).css({left : row.x + "%", top : row.y + "%"});
    }
    simple_tooltip(".points__item","tooltip");
  });
});

$(document).on('click', '#coordinate_show', function () {
  $(".points").css("display", "block");
  $('#coordinate_show').css("display", "none");
  $('#coordinate_hide').css("display", "block");
});

$(document).on('click', '#coordinate_hide', function () {
  $(".points").css("display", "none");
  $('#coordinate_hide').css("display", "none");
  $('#coordinate_show').css("display", "block");
});

$(document).on('click', '#animation', function () {
var items =  $('.armalit .popup');
var items2 = $('.askold .popup');
var items3 = $('.burevestnik .popup');
var items4 = $('.skbk .popup');
var show = (items) => {
   if(items.length > 0) {
     $([].shift.call(items)).fadeIn(1000).delay(3000).fadeOut(500, () => show(items))
   } else { return 'done'; } };

show(items);
show(items2);
show(items3);
show(items4);

// function popup_delay(district) {
//   var items = $("." + district + " .popup");
//   $(items).each(function(i, el) {
//       $(el).delay(1000 * i).fadeIn(2000);
//       $(el).delay(2000 * i).fadeOut(2000);
//   });
// }
// popup_delay("armalit");
// popup_delay("askold");
// popup_delay("burevestnik");
// popup_delay("skbk");

});
